// everyday-recommend/play-music/play-music.js
import request from '../../utils/ajax'
import PubSub from 'pubsub-js'

//获取全局状态
var appInstance = getApp()
Page({

  /**
   * 页面的初始数据
   */
  data: {
    //音乐是否在播放
    isPlay:false,
    MusicName:'',
    songer:'',
    musicId:'',
    musicUrl:'',
    songerHeader:''

  },
  //切换歌曲
  changeMusic(event){
    //如果有正在播放的歌曲就暂停播放
    this.MusicManger.stop()

    let type = event.currentTarget.id
    //订阅消息，接受新的musicId
    PubSub.subscribe('MusicId',(msg,MusicId)=>{
      console.log("收到返回的新歌曲ID",MusicId)
      this.setData({musicId:MusicId})
      //重新请求音乐的地址
      this.LoadMusic(MusicId)
      this.LoadMsicUrl(MusicId)
      //修改全局data的musicID和当前页面的musicid
      this.setData({musicId:MusicId})
      appInstance.musicId = MusicId
      console.log("appInstance:",appInstance.musicId)
      PubSub.unsubscribe('MusicId')
    })
   
    //发布消息,进行切换歌曲
    PubSub.publish('changeMusic',type) 
  },
  //用户点击播放暂停
  Play()
{   
    console.log("点击播放/暂停")
    let isPlay = this.data.isPlay
    // this.setData({isPlay:!isPlay})
    
    //音乐控制
    if(!isPlay){
      //播放音乐
      console.log("点击播放")
      // console.log("这是manger示例",MusicManger)
      this.MusicManger.src = this.data.musicUrl
      this.MusicManger.title = this.data.MusicName
    }else{
      //暂停播放
      console.log("点击暂停")
      this.MusicManger.pause()
    }
    
},
  //发送请求获取数据
  async LoadMusic(musicId){
    let music  = await request('/song/detail',{ids:musicId})
    console.log(music)
    this.setData({
      MusicName:music.songs[0].name,
      songer:music.songs[0].ar[0].name,
      songerHeader:music.songs[0].al.picUrl
      
    })
    wx.setNavigationBarTitle({title:this.data.MusicName})
  },
//发送请求获取音乐播放地址
  async LoadMsicUrl(musicId){
    let musicUrl = await request('/song/url',{id:musicId})
    this.setData({musicUrl:musicUrl.data[0].url})
  },
  //修改状态
  changeState(isPlay){
    this.setData({isPlay})
    appInstance.isPlay = isPlay
    console.log("成功修改状态为：",isPlay,this.data.isPlay)
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    //创建全局唯一的音乐管理变量
    this.MusicManger = wx.getBackgroundAudioManager()
    let musicId = options.musicId
    console.log(musicId)
    this.setData({musicId})
    //发送请求
    this.LoadMusic(musicId)
    this.LoadMsicUrl(musicId)
    //判断音乐状态
    if(appInstance.isPlay&&appInstance.musicId===musicId){
      //与打开的歌曲是同一首歌曲，直接修改状态
      this.setData({isPlay:true})
      this.MusicManger.Play()
    }
    if(appInstance.isPlay&&appInstance.musicId!=musicId){
      //暂停之前的歌曲
      this.MusicManger.stop()
    }
    /*
      出现bug,用户只操作功能栏中的播放和暂停是没有问题的，但是，当用户通过系统的播放暂停控制的时候，就会出现页面
      状态显示和音乐实际状态不符合的情况。这就要通过监听函数去修改页面的状态
    */ 
    //监听音乐的播放和暂停
    this.MusicManger.onPlay(()=>{
       this.changeState(true)
       appInstance.musicId = musicId
       console.log("监听到音乐播放")
    })
    this.MusicManger.onPause(()=>{
      this.changeState(false)
      console.log("监听到音乐暂停")
    })
    this.MusicManger.onStop(()=>{
      this.changeState(false)
      console.log("监听到音乐停止")
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})