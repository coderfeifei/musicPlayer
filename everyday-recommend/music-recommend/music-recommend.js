// everyday-recommend/music-recommend/music-recommend.js
import PubSub from 'pubsub-js'
import request from '../../utils/ajax'

Page({

  /**
   * 页面的初始数据
   */
  data: {
    date: '', //当前日期
    time: '', //当前时间
    musicList: [],
    index:0//当前的音乐索引
  },

  //点击事件
  goPlay(event){
    console.log("这是event",event)
    let musicId = event.currentTarget.id
    let index = event.currentTarget.dataset.index
    this.setData({index})
    //跳转到播放页面并传递当前要播放音乐的id
    wx.navigateTo({
      url: '/everyday-recommend/play-music/play-music?musicId='+musicId,
    })
  },
  //获取每日推荐
  async getRecommend(cookie){
    let musicList = await request('/recommend/songs',{},{cookie:cookie})
    console.log("这是每日推荐",musicList)
    this.setData({musicList:musicList.data.dailySongs})
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    //请求每日推荐需要用户登录，前提判断用户是否登录
    let cookie =(wx.getStorageSync('cookies'))[0]
    if(!cookie){
      wx.showToast({
        title: '请先登录',
        icon: 'none',
        success:()=>{
          wx.redirectTo({
            url: '/pages/login/login',
          })
        }
      })
    }
    setInterval(() => {
      let data = new Date()
      this.setData({
        date: data.toLocaleDateString(), //获取当前日期
        time: data.toLocaleTimeString() //获取当前时间
      })
    }, 1000);
    this.getRecommend(cookie)


    //订阅消息
    PubSub.subscribe('changeMusic',(msg,type)=>{
      if(type==='pre'){
        console.log("上一首")
        //上一首
        if(this.data.index===0){
         
        }else{
          this.setData({index:this.data.index-1})
        }
      }else{
        //下一首
        console.log("下一首")
        this.setData({
          index:(this.data.index+1)%this.data.musicList.length
        })
      }
      let MusicId = this.data.musicList[this.data.index].id
      console.log("musicList",this.data.musicList)
      //发布消息，通知播放页面
      PubSub.publish('MusicId',MusicId)
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})