// pages/searchMusic.js
import ajax from '../../utils/ajax'
import {
  debounce
} from '../../utils/debounce'
import {
  throttle
} from '../../utils/throttle'

let isTrue = true //节流标识
let timeout = null //防抖计时器
Page({

  /**
   * 页面的初始数据
   */
  data: {
    hotSearch: [], //热搜榜的数据
    maxSearch: '', //头部搜索推荐
    InputSearch: '', //要搜索的内容模糊匹配数据
    searchPacth: [], //返回来匹配数据

    RecentSearch: [] //历史搜索记录


  },
  //请求热搜榜的数据
  async GetHotSearch() {
    let hotSearch = await ajax('/search/hot/detail')
    console.log(hotSearch)
    this.setData({
      hotSearch: hotSearch.data,
      maxSearch: hotSearch.data[0].searchWord
    })
  },
  //获取模糊匹配列表
  async getPatchList() {
    //发送请求获取匹配数据
    let searchPacth = await ajax('/search', {
      keywords: this.data.InputSearch,
      limit: 10
    })
    //保存搜索的记录
    let RecentSearch = this.data.RecentSearch
    //如果没有输入数据就不用发送请求和修改数据
    if (!this.data.InputSearch) {
      this.setData({
        searchPacth: []
      })
      return
    }
    RecentSearch.push({
      name: this.data.InputSearch
    })
    console.log(searchPacth)
    this.setData({
      searchPacth,
      RecentSearch
    })
    //将历史搜索存在本地
    //异步方式
    // wx.setStorage({
    //   key:'RecentSearch',
    //   data:RecentSearch
    // })
    //同步方式
   wx.setStorageSync('RecentSearch', RecentSearch)
  },
  //监听输入
  setInput(event) {

    let search = event.detail.value.trim()
    console.log(search)
    this.setData({
      InputSearch: search
    })
    //节流
    /*if(!isTrue){
      return
    }
    isTrue = false
    setTimeout(()=>{
      this.getPatchList()
      isTrue = true
    },1000)*/
    //防抖
    clearTimeout(timeout) //持续点击就会清除计时器
    timeout = setTimeout(() => {
      this.getPatchList()
    }, 1000)

    //这里不要把发送请求的函数放到监听输入里面，有/bug
    //另外这样每改变一次输入就会发送一次请求，效率很低
    //this.getPatchList()
    //采用函数节流的方式或者是函数防抖的方式进行优化性能
    // debounce(this.getPatchList,1000)
    // console.log("-------")
  },
  /**
   * 生命周期函数--监听页面加载
   */
  //点击取消清空输入框
  cancle(){
    this.setData({
      InputSearch:'',
      searchPacth:[]
    })
  },
//点击 删除历史记录
deleteRecent(){
  wx.showModal({
    title: '提示',
    content: '删除历史记录？',
    success: (res)=> {
      if (res.confirm) {
        //wx.removeStorage('RecentSearch')
        wx.removeStorage({
          key: 'RecentSearch',
        })
        console.log("此时的this",this)
        this.setData({
          RecentSearch:[]
        })
        console.log("用户确定了")
      } else if (res.cancel) {
        console.log('用户点击取消')
      }
    }
  })
  
},

  onLoad: function (options) {
    //获取热搜榜数据
    this.GetHotSearch()
     //获取历史搜索记录
    let RecentSearch = wx.getStorageSync('RecentSearch')
    console.log(RecentSearch)
   if(RecentSearch){
      this.setData({RecentSearch})
   }else{
     console.log("RecentSearch是空")
   }
  
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})