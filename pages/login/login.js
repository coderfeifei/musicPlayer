// pages/login/login.js
import ajax from '../../utils/ajax'
Page({

  /**
   * 页面的初始数据
   */
  data: {
      phone:"",
      password:"",
      result :""
  },
bindinput(event){
  //console.log(event)
  if(event.detail.value){
    this.setData({
      [event.target.id]:event.detail.value
    })
  }
},
async login(){
  //前端验证
  let match_phone = /^1(3|4|5|6|7|8|9)\d{9}$/
  // console.log(this.data)
  // let {phone,password} = this.data
  // console.log(phone+"ddd")
  //console.log(this.data.phone)
  if(!this.data.phone||!this.data.password){
    console.log("用户名或密码不能为空！")
    return;
  }
  else if(!match_phone.test(this.data.phone)){
      //表达式不匹配表示手机号不合法
      wx.showToast({
        title: '手机号无效',
        icon: "none" 
      })
      return;
  }else{
    console.log("登录成功！")
  }

  //后端验证
  const re = await ajax('/login/cellphone',{ phone:this.data.phone,password:this.data.password}, {} ,true)
  console.log("这是返回的结果",re)
  this.setData({result:re.data})
  if(re.data.code==400){
    console.log("该用户不存在，请检查手机号是否正确或是否注册")
  }
  else if(re.data.code==501){
    console.log(re.msg)
  }
  else if(re.data.code==200){
    console.log("后端验证成功")
    wx.showToast({
      title: '登录成功',
    })
    //设置cookie
    wx.setStorageSync('cookies', re.cookies)
    //设置本地缓存，下次就不用登录
  wx.setStorageSync('userinfo', JSON.stringify(this.data.result))
    setTimeout(()=>{
      console.log("这是头像的地址",this.data.result.profile.avatarUrl)
      wx.reLaunch({
        url: "/pages/personal/personal?nickname="+this.data.result.profile.nickname+'&avatarUrl='+encodeURIComponent(JSON.stringify(this.data.result.profile.avatarUrl)),
      })
    },2000)
    
  }else{
    console.log("出现未知错误")
  }

   
 
},
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})