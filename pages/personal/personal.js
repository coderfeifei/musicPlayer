// pages/personal/personal.js
import ajax from '../../utils/ajax'
//监听手指触摸事件
let start = 0
let move = 0
Page({
bindtouchstart(event){
  start = event.touches[0].clientY;
  //开始没有动画，只有松手才有
  this.setData({
    transition:""
  })
},
bindtouchmove(event){
  move = event.touches[0].clientY - start;
  if(move<=0){
    return;
  }
  else if(move>80)
  {
    move  = 80
  }
  this.setData({
    transform:`translateY(${move}rpx)`
  })
},
bindtouchend(event){ 
  this.setData({
    transform:`translateY(0rpx)`,
    transition:`transform 1s linear`
  })
},
//点击登录
tologin(){
  setTimeout(()=>{
    wx.redirectTo({
      url: '/pages/login/login',
    },1000)
  })
},
//获取用户的最近播放记录
async getRecentPlay(userid){
  let recentPlay = await ajax('/user/record',{uid:userid,type:1})
  let index = 0
  this.setData({
    recentPlay : recentPlay.weekData.splice(0,10).map((item)=>{
      item.id = index++
      return item
    })
  })
  console.log("这是用户的播放记录",this.data.recentPlay)
  console.log("ddd",this.data.recentPlay[0].song.al.picUrl)
},
  /**
   * 页面的初始数据
   */
  data: {
    transform :"translateY(0)",
    transition:"",
    nickname: "游客",
    avatarUrl:"../../static/person-images/missing-face.png",
    recentPlay:[],
    userinfo:{}
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    //这是通过跳转url中的数据传值
  //   console.log("这是option",options)
  //   let avatarUrl= JSON.parse(decodeURIComponent(options.avatarUrl))
  //   //nick_and_avatarUrl=
  //  // console.log("hahha",avatarUrl)
  //   this.setData({
  //     nickname : options.nickname,
  //     avatarUrl:avatarUrl
  //   })
  //   console.log(this.data.avatarUrl)
  //下面通过设置本地缓存来读取数据
  let userinfo
    try{
      userinfo = JSON.parse(wx.getStorageSync('userinfo'))
      console.log(userinfo)
      this.setData({userinfo:userinfo})
    }
    catch(error){
      console.log("userinfo的内容是空的")
      return
    }
    //正常读取到缓存数据
    
    this.setData({
          nickname : userinfo.profile.nickname,
          avatarUrl:userinfo.profile.avatarUrl
        })
    //读取用户的播放记录
    this.getRecentPlay(this.data.userinfo.account.id)
    console.log("读取用户播放记录成功！")
  
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})