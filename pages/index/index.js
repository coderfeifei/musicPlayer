import ajax from '../../utils/ajax'

Page({

  /**
   * 页面的初始数据
   */
  data: {
    banners:[],
    recommendList:[],
    topList:[]
  },
  goRecommend(){
      wx.redirectTo({
        url: '/everyday-recommend/music-recommend/music-recommend',
      })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: async function () {
    //发送请求请求数据
    //请求排行榜音乐
      //接口发送一次请求（参数不同）请求一种，现在我们请求五种
    let index = 0;
    const topList = []
    while(index<5){
      let result  = await ajax('/top/list',{idx:index})
      let mylist = {name:result.playlist.name,tracks:result.playlist.tracks.slice(0,3)}
      topList.push(mylist)
      index++
    }
    //更新数据
    this.setData({topList})
    console.log("这是toplist",this.data.topList)
    //请求推荐列表
    // const {result} = await ajax('/personalized',{limit:10})
    // console.log(result)
    // if(result){
    //   this.setData({recommendList:result})
    // }
    ajax('/personalized',{limit:10}).then(res=>{
      console.log("这是返回的结果推荐列表",res)
      console.log("--------------")
      this.setData({recommendList:res.result})
    })
    //会输出undefine,
    console.log("这是当前的recommendlist",this.recommendList)
    //请求轮播图内容
    const {banners} =  await ajax('/banner',{type:2})
    console.log(banners)
    if(banners){
      //获取到值就更新轮播图
      this.setData({banners})
    }
    //方法2
    //封装后返回的是promise对象
   /*ajax('/banner',{type:2}).then(res=>{
     console.log("请求回来的数据",res)
     this.setData({banners:res.banners})
   })*/
   //方法3
   /* wx.request({
      url: 'http://localhost:3000/banner',
      data:{type:2},
      success:(res)=>{
        console.log(res.data)
        this.setData({banners:res.data.banners})
      },
      fail:(error)=>{
        console.log(error);
      }
    })*/
    
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
    
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
    
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
    
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
    
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
    
  }
})