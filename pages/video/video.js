// pages/video/video.js
import ajax from '../../utils/ajax'
Page({

  /**
   * 页面的初始数据
   */
  data: {
    //视频标签
      videoTag:[{},{}],
    //每个标签对应的视频列表
      videoList:[{},{}],
      //点击的标签/id
      tag :'scroll'+0,
      videoListID:0,
      PrevideoID:''
  },
  //解决多个视频同时播放的问题
    //找到上一个正在播放的视频，暂停上一个
    //播放当前的视频
    //但是要判断是否是同一个视频，这点需要注意
handlePlay(event){
  //当前要播放视频的ID
  let videoID = event.currentTarget.id
  console.log(videoID)
  //判断是否正在有视频播放
  if(this.data.PrevideoID)
  {
    //有正在播放的视频
    //判断和上一个视频是否是同一个视频
    if(this.data.PrevideoID != videoID)
    {
      //不是同一个视频就暂停上一个视频播放
      wx.createVideoContext(this.data.PrevideoID).stop()
      //播放当前视频
      wx.createVideoContext(videoID).play()
      //更新prevideoID
      this.setData({
        PrevideoID:videoID
      })
    }
  }
  //没有正在播放的视频就直接保存videoID
  else{
    this.setData({
      PrevideoID:videoID
    })
  }
},
  //点击对应的导航切换对应的视频
click(event){
    let tag = event.currentTarget.id
    //let videoListID = tag.replace(/[^0-9]/)
    //console.log(videoListID)
    console.log(event)
    this.setData({
      //或者写成targetId*1,目的是将字符串转成数字
      tag :tag,
      videoListID :tag.replace(/[^\d]/g,"")*1
    })
  },
//发送请求获取视频列表
async getVideoList(){
  let videoTag = await ajax("/video/group/list")
  let t = 0
  this.setData({
    videoTag:videoTag.data.splice(0,10).map((item)=>{
      item.num = t++
      return item
    })
  })
  console.log("获取到的视频list",this.data.videoTag)
  this.setData({targetId :'scroll'+this.data.videoTag[0].id})
 //获取到视频标签名之后获取每个标签下对应的视频
  let videoList =[]
  let index = 0
  let limite = this.data.videoTag.length
  let cookie =(wx.getStorageSync('cookies'))[0]
  
  while (index<limite) {
    let temp = await ajax('/video/group',{id:this.data.videoTag[index].id},{cookie:cookie})
    index++
    videoList.push(temp)
    // console.log("----")
    // console.log(temp)
    // console.log(videoList)
  }
  this.setData({
    videoList:videoList
  })
  console.log("成功设置视频合集",this.data.videoList)
 
   
},
//点击搜索歌曲跳转到搜索页面，可以回退
search(){
  wx.navigateTo({
    url: '/pages/searchMusic/searchMusic',
  })
},

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.getVideoList()
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})