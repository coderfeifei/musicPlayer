    /*
    * 节流函数
    * @param fn 事件触发的操作
    * @param delay 间隔多少毫秒需要触发一次事件
    */
    //基本原理
    export function throttle(fn, delay) {
      let valid = true;
      return function() {
        let context = this;
        let args = arguments;
        if (!valid) {
          return;
        }
        valid = false;
        setTimeout(() => {
          fn.apply(context, args);
          valid = true;
        }, delay);
      }
    }