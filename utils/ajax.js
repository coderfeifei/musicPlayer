import {Port,testPort,testPort2} from "./serverPort"
export default(url,data,header=null,isLogin = false ,method = "GET")=>{
    //异步任务用promise比较好做
   return new Promise((resovle,reject)=>{
    wx.request({
        url: Port+url,
        data,
        header,
        method,
        success: (res)=>{
            console.log("isLogin",isLogin)
            if(isLogin){
               resovle(res)
               console.log("这是登录的----")
               return
            }
            console.log(res.data)
            console.log("这不是登录")
           resovle(res.data)
        },
        fail:(error)=>{
            console.log(error)
            console.log("出错了")
            reject(error)
        }
      })
   }) 
}