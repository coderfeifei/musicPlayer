 //函数的防抖：多次频繁点击不会执行，只有在冷静下来（最后一次执行）
 export function debounce(func, delay) {
   let timeout = null
   console.log(func,delay)
  // func.apply(this, arguments)
   return function () {
     console.log("我也执行了")
     clearTimeout(timeout) // 如果持续触发，那么就清除定时器，定时器的回调就不会执行。
     timeout = setTimeout(() => {
       func.apply(this, arguments)
     }, delay)
   }
 }